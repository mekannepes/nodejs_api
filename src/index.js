require("reflect-metadata")
const express = require("express")
const typeorm = require("typeorm")
const apiRouter = require("./routers/Api")
const Music = require("./models/Music").Music
const User = require("./models/User").User

const app = express()
const PORT = process.env.PORT || 5000

app.use(express.json())
app.use(express.urlencoded({extended:false}))

//address => localhost:5000/api/
app.use("/api",apiRouter)

//method => GET; address => localhost:5000/ 
app.get("/", (req,resp) => {
    resp.send("Mekan Nepesov")
})

//creating connection to database mongodb
typeorm.createConnections([
    {
        name:"mongoDB",
        type: "mongodb",
        "host": "127.0.0.1",
        "port": "27017",
        "username": "mekannepes",
        "password": "programmer",
        "database": "test",
        "synchronize": true,
        "logging": false,
        "entities": [
           require("./entity/UserSheme"),
           require("./entity/MusicSheme")
        ]
    },
    {
        name:"postgresDB",
        type:"postgres",
        "host": "localhost",
        "port": "5432",
        "username": "postgres",
        "password": "mekannepes",
        "database": "test",
        "synchronize": true,
        "logging": false,
        "entities": [
           require("./entity/UserPostgresSheme"),
           require("./entity/MusicPostgresSheme"),
           require("./entity/AuthorPostgresSheme")
        ]
    }
]).then(async ()=>{
    console.log("Database connected!")


    //starting server
    app.listen(PORT,()=>{
        console.log(`Server has been started on port ${PORT}`)
    })
}).catch(err=>{
    console.log(`Error: ${err}`)
})

// createConnection().then(async connection => {

//     console.log("Inserting a new user into the database...");
//     const user = new User();
//     user.firstName = "Timber";
//     user.lastName = "Saw";
//     user.age = 25;
//     await connection.manager.save(user);
//     console.log("Saved a new user with id: " + user.id);

//     console.log("Loading users from the database...");
//     const users = await connection.manager.find(User);
//     console.log("Loaded users: ", users);

//     console.log("Here you can setup and run express/koa/any other framework.");

// }).catch(error => console.log(error));
