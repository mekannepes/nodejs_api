class Music {
    constructor(id,_name,_address,_author){
        this._id = id,
        this.name = _name,
        this.address = _address,
        this.author = _author
    }
}

module.exports = {
    Music:Music
}