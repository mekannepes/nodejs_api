const jwt = require("jsonwebtoken")
const jwtSecret = require("../configs/app")

module.exports = (req,res,next) => {
    const autHeader = req.get("Authorization")

    if(!autHeader){
        return res.status(401).json({
            "error":"error",
            "message":"Token not provided!"
        })        
    }
    const token = autHeader.replace('Bearer ','')
    try {
        jwt.verify(token,jwtSecret)
    } catch (err) {

        if(err instanceof jwt.JsonWebTokenError){
            return res.status(401).json({
                "error":"error",
                "message":"Invalid token"
            })
        }    
    }
     
    next()
}