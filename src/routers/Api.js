const express = require("express")
const typeorm = require("typeorm")
const User = require("../models/User").User
const Music = require("../models/Music").Music
const Author = require("../models/Author").Author
const {jwtSecret} = require("../configs/app")
const jsonWebToken = require("jsonwebtoken")
const authMiddlewear = require("../middlewear/auth")

const router = express.Router()

router.post("/register",async (req,res) => {

    const {name,email,password} = req.body
    const connection = typeorm.getConnection("mongoDB")
    const manager = connection.getMongoManager()
    const repository = manager.getMongoRepository(User)
    try {
        let user = await repository.findOne({email})

        if(!user) {

            user = new User()
            user.name = name
            user.email = email
            user.password = password

            console.log("create user")
            try {
                let u = await repository.save(user)
                console.log("Saved")
                return res.status(200).json({
                    status:"ok", 
                    message:""
                })

            } catch (err) {
                console.log("don't save")
                    //здесь появляется ошибка createValueMap
                    //console.log(err)
                    return res.status(401).json({
                        status:"error", 
                        message:"don't save"
                    })
            }

        }else{
            console.log("user exists")
            
            return res.status(401).json({
                status:"error", 
                message:"User already exists!"
            })      
        }

    } catch (err) {
        console.log("Nepesow")
        console.log(err)  
    }

    console.log("end")
    
})

router.post("/login",async (req,res) => {

    const {email,password} = req.body

    const connection = typeorm.getConnection("mongoDB")

    const repository = connection.getMongoRepository(User)
    try {

        let user = await repository.findOne({email})
        
        if(!user){
            return res.status(401).json({
                status:"error",
                message:"User not found!"
            })            
        }
        console.log(user)

        if(password == user.password){
            const token = jsonWebToken.sign(user.email,jwtSecret)
            return res.json({
                "token":token
            })
            
        }else{
            return res.status(401).json({
                status:"error",
                message:"wrong password!"
            })
        }

    } catch (err) {
        console.log("DB catch");
        console.log(err)
        return res.json({
            "error":"error"
        })
    }

})

router.get("/musics",authMiddlewear,async (req,res) => {
    const autHeader = req.get("Authorization")

    const token = autHeader.replace("Bearer ","")

    const connection = typeorm.getConnection("mongoDB")

    const musicRepository = connection.getMongoRepository(Music)

    try {

        let musics = await musicRepository.find()

        return res.status(200).json(musics)

    } catch (err) {
        console.log("Error")
        //console.log(err)

        return res.status(401).json({
            "error":"Error",
            "message":"Exeptions DB"
        })
    }
})
router.post("/music",authMiddlewear, async (req,res) => {
    const {name} = req.body

    const autHeader = req.get("Authorization")

    const token = autHeader.replace("Bearer ","")

    let email = jsonWebToken.decode(token)

    const connection = typeorm.getConnection("mongoDB")

    const userRepository = connection.getMongoRepository(User)
    const musicRepository = connection.getMongoRepository(Music)
    try {
        let user = await userRepository.findOne({email},{name,email})
        console.log(user)
        let music = new Music()
        music.name = name
        music.address="here must be address"
        music.author = {
            name:user.name,
            email:user.email
        }

        await musicRepository.save(music)
        console.log("Saved")

        return res.status(200).json({
            "error":"",
            "message":"Music added"
        })

    } catch (err) {
        console.log("Error")
        //console.log(err)

        return res.status(401).json({
            "error":"Error",
            "message":"Music didn't add"
        })
    }

})

router.get("/musicstopq",authMiddlewear,async(req,res)=>{

    //connections mongodb
    const connectionMongo = typeorm.getConnection("mongoDB")

    //connection postgres
    const connectionPQ = typeorm.getConnection("postgresDB")

    const musicRepositoryPQ = connectionPQ.getRepository(Music)

    const musicRepository = connectionMongo.getMongoRepository(Music)

    try {

        let musics = await musicRepository.find()

        let musicsforPQ = musics.map(m=>{
            let music = new Music()
            music.name = m.name
            music.address = m.address
            music.author = {
                name:m.author.name,
                email:m.author.email
            }
            return music
        })


        await musicRepositoryPQ.save(musics)

        return res.status(200).json(musicsforPQ)

    } catch (err) {
        console.log("Error")

        return res.status(401).json({
            "error":"Error",
            "message":"Exeptions DB"
        })
    }

})

router.get("/userstopq",authMiddlewear,async (req,res)=>{

    const connectionMongo = typeorm.getConnection("mongoDB")

    //connection postgres
    const connectionPQ = typeorm.getConnection("postgresDB")

    const userRepositoryPQ = connectionPQ.getRepository(User)

    const userRepositoryMongo = connectionMongo.getMongoRepository(User)

    try {
        
        let users = await userRepositoryMongo.find()
        await userRepositoryPQ.save(users)
        return res.status(200).json(users)

    } catch (error) {
        console.log("Error")

        return res.status(401).json({
            "error":"Error",
            "message":"Exeptions DB"
        })

    }


})

module.exports = router