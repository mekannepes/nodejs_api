const EntitySchema = require("typeorm").EntitySchema
const Music = require("../models/Music").Music

module.exports = new EntitySchema({
    name:"Music",
    target:Music,
    columns:{
        id:{
            primary:true,
            generated:true,
            type:"integer",
        },
        name:{
            type:"character varying"
        },
        address:{
            type:"character varying"
        },
    },
    relations:{
        authors:{
            target:"Author",
            type:"many-to-many",
            joinTable:true,
            cascade:true
        }
    }
    
})