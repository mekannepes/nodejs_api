const EntitySchema = require("typeorm").EntitySchema
const typeorm = require("typeorm")
const User = require("../models/User").User

module.exports = new  EntitySchema( {
    name:"User",
    target:User,
    columns:{
        _id:{
            primary:true,
            type:String,
            generated:true,
        },
        name:{
            type:String
        },
        email:{
            type:String
        },
        password:{
            type:String
        }
    }
})