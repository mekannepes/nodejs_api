const EntitySchema = require("typeorm").EntitySchema
const Music = require("../models/Music").Music

module.exports = new EntitySchema({
    name:"Music",
    target:Music,
    columns:{
        _id:{
            primary:true,
            generated:true,
            type:"int",
        },
        name:{
            type:"varchar"
        },
        address:{
            type:"varchar"
        },
        author:{
            type:Object
        }
    },
    
})