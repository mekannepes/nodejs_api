const EntitySchema = require("typeorm").EntitySchema
const typeorm = require("typeorm")
const User = require("../models/User").User

module.exports = new  EntitySchema( {
    name:"User",
    target:User,
    columns:{
        id:{
            primary:true,
            type:"integer",
            generated:true,
        },
        name:{
            type:"character varying"
        },
        email:{
            type:"character varying"
        },
        password:{
            type:"character varying"
        }
    }
})