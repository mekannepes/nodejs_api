const EntitySchema = require("typeorm").EntitySchema
const typeorm = require("typeorm")
const Author = require("../models/Author").Author

module.exports = new  EntitySchema( {
    name:"Author",
    target:Author,
    columns:{
        id:{
            primary:true,
            type:"integer",
            generated:true,
        },
        name:{
            type:"character varying"
        },
        email:{
            type:"character varying"
        }
    }
})